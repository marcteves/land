Required program: git
Required commands: git checkout, git clone, git branch


SETUP
1. Open your terminal with git installed
2. Navigate to a folder where you want the directory containing the repo to be created
3. Enter the command 'git clone https://gitlab.com/marcteves/land.git'
4. Enter the command 'cd land'
5. If you want to use git commands on the 'land' repo, enter them while this is your working directory (the directory your terminal is currently inside of)

MOVING BETWEEN BRANCHES
1. Enter the command 'git branch'. This will display all the branches in the repository. This one contains 'master' and 'solution'.
2. Navigate to the branch you want with 'git checkout <branch name>'
3. Note that almost every git command applies only to the branch you are in.
